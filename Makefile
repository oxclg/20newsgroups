TOKENIZE=tokenize-anything.sh

all: labelled_train labelled_dev labelled_test

data: 
	python scripts/write_dataset.py

trainsplit:
	for i in train/*; do python scripts/devsplit.py $$i; done

rawtest: data
	cat test/* > rawtest

rawtrain: data trainsplit
	cat train/*.train > rawtrain

rawdev: data trainsplit
	cat train/*.dev > rawdev

labelled_train: rawtrain
	cat rawtrain | cut -d ' ' -f 1 > labelled_train.labels
	cat rawtrain | cut -d ' ' -f 2- | $(TOKENIZE) > labelled_train.docs

labelled_dev: rawdev
	cat rawdev | cut -d ' ' -f 1 > labelled_dev.labels
	cat rawdev | cut -d ' ' -f 2- | $(TOKENIZE) > labelled_dev.docs

labelled_test: rawtest
	cat rawtest | cut -d ' ' -f 1 > labelled_test.labels
	cat rawtest | cut -d ' ' -f 2- | $(TOKENIZE) > labelled_test.docs

clean-all:
	rm -rd train test
	rm rawdev rawtrain rawtest
	rm labelled_train.docs labelled_train.labels
	rm labelled_test.labels labelled_test.docs
	rm labelled_dev.labels labelled_dev.docs
	
clean-raw:
	rm rawdev rawtrain rawtest