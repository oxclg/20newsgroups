# -*- coding: utf-8 -*-

import os
import re
import os.path as path

from sklearn.datasets import fetch_20newsgroups

import sys
from HTMLParser import HTMLParser

class MLStripper(HTMLParser):
    def __init__(self):
        self.reset()
        self.fed = []
    def handle_data(self, d):
        self.fed.append(d)
    def get_data(self):
        return ''.join(self.fed)

def strip_tags(html):
    s = MLStripper()
    s.feed(html)
    return s.get_data()

def cleanline(line):
    no_html = strip_tags(line)
    no_new_lines = no_html.replace('\n',' ').replace('\r', ' ')
    no_extra_spaces = re.sub(' +', ' ', no_new_lines).strip()
    return no_extra_spaces

def main():
    OUTPUTDIR = "."

    ng_train = fetch_20newsgroups(subset='train', remove=('headers', 'footers', 'quotes'), shuffle=True)
    ng_test = fetch_20newsgroups(subset='test', remove=('headers', 'footers', 'quotes'), shuffle=True)

    train_targetnames = ng_train.target_names
    test_targetnames = ng_test.target_names

    train_data_zip = zip([train_targetnames[x] for x in ng_train.target], [cleanline(x) for x in ng_train.data])
    test_data_zip = zip([test_targetnames[x] for x in ng_test.target], [cleanline(x) for x in ng_test.data])

    trainpath = path.join(OUTPUTDIR, "train")
    if not path.exists(trainpath):
        os.makedirs(trainpath)
        
    testpath = path.join(OUTPUTDIR, "test")
    if not path.exists(testpath):
        os.makedirs(testpath)

    traingroupfiles = {}
    for target in train_targetnames:
        traingroupfiles[target] = open(path.join(trainpath, target), 'w')
    testgroupfiles = {}
    for target in test_targetnames:
        testgroupfiles[target] = open(path.join(testpath, target), 'w')

    for group, doc in train_data_zip:
        if len(doc) < 1: 
            continue
        output = "%s %s\n" % (group, doc)
        traingroupfiles[group].write(output.encode('utf-8'))
        
    for group, doc in test_data_zip:
        if len(doc) < 1: 
            continue
        output = "%s %s\n" % (group, doc)
        testgroupfiles[group].write(output.encode('utf-8'))    

    for f in traingroupfiles.values():
        f.close()
        
    for f in testgroupfiles.values():
        f.close()

if __name__ == '__main__':
    main()
