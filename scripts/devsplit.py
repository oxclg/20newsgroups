#!/usr/bin/env python

import sys

def main(filename):
    devfn = "%s.dev" % filename
    trainfn = "%s.train" % filename
    lines = open(filename).readlines()
    filelen = len(lines)
    devsize = filelen // 10

    with open(devfn, 'w') as f:
        for line in lines[:devsize]:
            f.write(line)

    with open(trainfn, 'w') as f:
        for line in lines[devsize:]:
            f.write(line)

if __name__ == "__main__":
    if len(sys.argv) < 2:
        sys.exit(1)
    main(sys.argv[1])