# Outline

This is a cleaned and reformatted version of the [20 Newsgroups Dataset](http://qwone.com/~jason/20Newsgroups/). Aside from the change in format (single documents for testing and training, instead of folders of documents), this dataset includes a new training/development set split, obtained from the original training dataset.

# Construction

The labelled training set has been obtained from `scikit-learn.datasets` using the `scripts/write_dataset.py` (headers, footers and quotes are stripped) and split into a new training set (90% of the original training set) and a development set (the remaining 10% of the original dataset) using `scripts/devsplit.py`. Both the new training and development sets are balanced for group label (preserving the proportion of group messages from the original training set).

Each document has been cleaned using a custom html stripping script, and tokenised using `tokenize-anything.sh` from [cdec](https://github.com/redpony/cdec).

# Files and Formats
This project contains the following files:

* `./labelled_train.labels` and `./labelled_train.docs` contain the aligned labels and documents (one per line) from the new training set obtained from 90% of the 20 newsgroups original training set, such that the `k`th line of `./labelled_train.labels` is the group label for the document on the `k`th line of `./labelled_train.docs`.
* `./labelled_dev.labels` and `./labelled_dev.docs` contain the aligned labels and documents (one per line) from the new training set obtained from  the 10% of 20 newsgroups original training set not used in the new training set, such that the `k`th line of `./labelled_dev.labels` is the group label for the document on the `k`th line of `./labelled_dev.docs`.
* `./labelled_test.labels` and `./labelled_test.docs` contain the aligned labels and documents (one per line) from 20 newsgroups original test set, such that the `k`th line of `./labelled_test.labels` is the group label for the document on the `k`th line of `./labelled_test.docs`.
* `./scripts`
* `./test`
* `./train`
* `./Makefile` allows the whole dataset to be rebuilt with `make TOKENIZE=/path/to/tokenize-anything.sh`.
* `./README.md` is this file, outlining the origin and construction of the reformatted dataset, and the file formats therein.

# Further details

This reformatted dataset was produced by [Ed Grefenstette](http://www.egrefen.com), based on the version of [20 Newsgroups Dataset](http://qwone.com/~jason/20Newsgroups/) obtained on Dec 1 2013, and was last updated on Dec 3 2013.